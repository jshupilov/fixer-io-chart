'use strict'

module.exports = {
    "up": `CREATE TABLE IF NOT EXISTS symbols (
        id INT NOT NULL AUTO_INCREMENT,
        code VARCHAR(3) NOT NULL,
        name VARCHAR(45) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE INDEX code_UNIQUE (code ASC));`,
    "down": "DROP TABLE IF EXISTS symbols;"
}