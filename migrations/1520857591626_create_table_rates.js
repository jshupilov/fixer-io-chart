'use strict'
module.exports = {
    "up": `CREATE TABLE IF NOT EXISTS rates (
            id INT NOT NULL AUTO_INCREMENT,
            base VARCHAR(3) NOT NULL,
            dtime DATETIME NOT NULL,
            symbols VARCHAR(3) NOT NULL,
            rate FLOAT NOT NULL,
            PRIMARY KEY (id),
            INDEX DATE (dtime ASC));
        `,
    "down": "DROP TABLE IF EXISTS rates;"
}