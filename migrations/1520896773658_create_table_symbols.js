'use strict'
var fixer = require('../app/services/fixerApiRest');
var _ = require('underscore');

function validateAndFormatSymbols(data, callback) {
    if (_.isObject(data)) {
        if (!_.isEmpty(data)) {
            if (_.has(data, 'success') && data.success) {
                if (_.has(data, 'symbols') && _.isObject(data.symbols)) {
                    if (!_.isEmpty(data.symbols)) {
                        var symbols = _.map(data.symbols, (val, key) => `("${key}", "${val}")`);
                        return callback(null, symbols);
                    }
                }
            }
        }
    }
    return callback(true);
}
module.exports = {
    "up": (conn, callback) => {
        fixer.symbols((err, res) => {
            if (err) {
                return callback();
            }
            validateAndFormatSymbols(res, (err, symbols) => {
                if (err) {
                    return callback();
                }
                var sql = 'INSERT INTO symbols(code, name) VALUES ' + symbols.join(",");
                conn.query(sql, callback());
            });
        });
    },
    "down": "TRUNCATE TABLE symbols"
}