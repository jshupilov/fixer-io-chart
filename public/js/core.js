var app = angular.module('fixerRates', [
    'ratesController',
    'ratesService',
    'symbolsService',
    'angularMoment',
    'chart.js',
    'ngMessages',
    'ui.bootstrap'
]);

app.directive('dateValid', ($q) => {
    return {
        require: 'ngModel',
        link: (scope, element, attr, ngModel) => {
            ngModel.$asyncValidators.invalidDate = function(modelValue, viewValue) {
                var date = Date.parse(viewValue);
                if (isNaN(date)) {
                    return $q.reject();
                }
                return $q.resolve();
            }
        }
    }
});