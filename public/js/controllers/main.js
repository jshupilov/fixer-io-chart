'use strict';
angular.module('ratesController', [])

.controller('mainController', ['$scope', '$http', 'Rates', 'moment', 'Symbols',

    function($scope, $http, Rates, moment, Symbols) {
        var dateLimit = date => {
            date.setMonth(new Date(date).getMonth() - 1);
            return new Date(date);
        };

        $scope.view = {
            title: 'Fixer Io Currency Rates Chart',
            errorMsg: false,
            errorRequiredMsg: 'You did not enter a date',
            loading: true,
            drawChartBtnDisable: false,
            drawChartBtnText: 'Draw Chart',
            swapBtnText: 'Swap',
            dateFormat: 'yyyy-MM-dd',
            startDate: dateLimit(new Date()),
            endDate: new Date(),
            calEndOpened: false,
            calStartOpened: false,
            calStartOptions: {
                dateDisabled: disabled,
                startingDay: 1
            },
            calEndOptions: {
                dateDisabled: disabled,
                startingDay: 1,
                maxDate: new Date()
            },
            base: 'EUR',
            symbol: 'USD',
            currentRate: 'EUR / USD',
            labels: [],
            chart: [],
            series: []
        };

        function disabled(data) {
            var date = data.date,
                mode = data.mode;
            return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
        }

        $scope.$watch("view.startDate", (newValue, oldValue) => {
            let val = new Date(newValue || oldValue);
            val.setMonth(new Date(val).getMonth() + 1);
            $scope.view.calEndOptions.minDate = val;
        });

        $scope.$watch("view.endDate", (newValue, oldValue) => {
            let val = new Date(newValue || oldValue);
            $scope.view.calStartOptions.maxDate = dateLimit(val);
        });

        $scope.calEndOpen = () => $scope.view.calEndOpened = true;
        $scope.calStartOpen = () => $scope.view.calStartOpened = true;

        $scope.swapSymbols = () => {
            let t = $scope.view.base;
            $scope.view.base = $scope.view.symbol;
            $scope.view.symbol = t;
        };

        $scope.drawChart = () => {
            $scope.view.drawChartDisable = true;
            $scope.view.drawChartBtnText = 'Drawing';
            var query = {
                start_date: moment($scope.view.startDate, 'YYYY-MM-DD').format('YYYY-MM-DD'),
                end_date: moment($scope.view.endDate, 'YYYY-MM-DD').format('YYYY-MM-DD'),
                base: $scope.view.base,
                symbols: $scope.view.symbol
            };
            Rates.get(query)
                .success((res) => {
                    if (Object.keys(res).length > 0) {
                        if (res.data) {
                            $scope.view.labels = res.data.map((row) => moment(row.date, 'YYYY-MM-DD').format('YYYY-MM-DD'));
                            $scope.view.chart = res.data.map((row) => row.rate);
                            $scope.view.errorMsg = false;
                            $scope.view.loading = false;
                        } else {
                            $scope.view.chart = [];
                            $scope.view.labels = [];
                            $scope.view.errorMsg = res.error;
                        }
                    } else {
                        $scope.view.chart = [];
                        $scope.view.labels = [];
                        $scope.view.errorMsg = res.error;
                    }
                    $scope.view.currentRate = $scope.view.base + ' / ' + $scope.view.symbol;

                    $scope.view.drawChartDisable = false;
                    $scope.view.drawChartBtnText = 'Draw Chart';
                });
        };


        Symbols.get()
            .success((res) => {
                if (Object.keys(res).length > 0) {
                    if (res.data) {
                        $scope.view.symbols = res.data;
                        $scope.view.errorMsg = false;
                        $scope.drawChart();
                    } else {
                        $scope.view.errorMsg = res.error;
                    }
                }
            });

    }
]);