angular.module('symbolsService', [])

.factory('Symbols', ['$http', function($http) {
    return {
        get: function() {
            return $http.get('/api/symbols');
        }
    }
}]);