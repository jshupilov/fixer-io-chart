angular.module('ratesService', [])

.factory('Rates', ['$http', function($http) {
    return {
        get: function(query) {
            var options = {
                params: query,
                headers: { 'Accept': 'application/json' }
            };
            return $http.get('/api/rates', options);
        }
    }
}]);