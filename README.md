# Node Fixer Io Currience Rates Chart

A Node app built with Mysql and Angular.

## Requirements

- [Node and npm](http://nodejs.org)
- Mysql: Make sure you have your own local or remote Mysql database configured in `.env`

## Installation
1. Clone the repository: `git clone https://jshupilov@bitbucket.org/jshupilov/fixer-io-chart.git`
2. Install the application: `npm install`
3. Create `.env` file with your own MySql config and Fixer Io authenticate details
```
    DB_HOST=localhost
    DB_USER=demo
    DB_PASSWORD=demodemo
    DB_NAME=currencies_rates
    FIXER_API_URL=http://data.fixer.io/api/
    FIXER_API_KEY=9dea3bfe2837c9806c96b693715aa30e
    PORT=8080
```
4. Run database migration script `npm run-script migration-up`
5. Start the server: `node app.js`
6. View in browser at `http://localhost:8080`

