'use strict'
var morgan = require('morgan');
var express = require('express');
var app = express();
//var database = require('./config/database');
var bodyParser = require('body-parser');
var port = process.env.PORT || 8080;
var dotenv = require('dotenv');
var methodOverride = require('method-override');
dotenv.config();

app.use(express.static('./public'));
app.use('/favicon', express.static('./favicon'));
app.use('/modules', express.static('./node_modules'));
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(methodOverride('X-HTTP-Method-Override'));

// routes ======================================================================
require('./app/routes.js')(app);

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port", port);