'use strict';
var mysql = require('../../config/database');
var _ = require('underscore');

function parseQuery(query) {
    return `dtime >= '${query.start_date}' AND dtime < '${query.end_date}' 
        AND base = '${query.base}' AND symbols = '${query.symbols}'`;
}

function formatFixerData(data, query) {

    return _.map(data, (row) => {
        return `('${row.base}', '${row.date}', '${query.symbols}', ${row.rates[query.symbols]})`;
    });
}
module.exports = mysql.extend({
    tableName: "rates",

    getRates(query, callback) {
        var conditions = {
            where: parseQuery(query),
            fields: [
                'base',
                'DATE_ADD(dtime, INTERVAL(-WEEKDAY(dtime)) DAY) AS date',
                'symbols',
                'round(avg(rate), 5) AS rate'
            ],
            group: ['base', 'date', 'symbols']
        }
        this.find('all', conditions, callback);
    },

    bulkInsertFixer(data, query, callback) {
        var rates = formatFixerData(data, query);
        var sql = 'INSERT INTO rates(base, dtime, symbols, rate) VALUES ' + rates.join(',')
        this.query(sql, err => {
            if (err) {
                return callback(err);
            }
            this.getRates(query, callback);
        });
    }
});