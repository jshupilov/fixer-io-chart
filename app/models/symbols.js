'use strict';
var mysql = require('../../config/database');

module.exports = mysql.extend({
    tableName: "symbols",
});