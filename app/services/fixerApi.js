'use strict';
var fixer = require('fixer-api');
var _ = require('underscore');
var q = require('q');
var moment = require('moment');
var Rate = require('../models/rates');

function parseQuery(query) {
    return _.pick(query, 'base', 'symbols');
}

function validateResponse(data, query) {
    if (_.isObject(data) && !_.isEmpty(data)) {
        const keys = ['base', 'date', 'rates'];
        for (let i in keys) {
            var key = keys[i];
            if (!_.has(data, key)) {
                return false;
            }
        }

        if (_.isString(data.base) && data.base.length === 3) {
            if (_.isDate(new Date(data.date))) {
                if (_.isObject(data.rates) && !_.isEmpty(data.rates)) {
                    if (_.has(data.rates, query.symbols)) {
                        if (_.isNumber(data.rates[query.symbols])) {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return false;
}

exports.byPeriod = (query, callback) => {
    const startDate = moment(query.start_date);
    const endDate = moment(query.end_date);
    const pQuery = parseQuery(_.clone(query));
    var promises = [];
    var rates = [];
    var byDate = (date, pQuery, query, p) => {
        fixer.forDate(date, pQuery).then((data) => {
            if (validateResponse(data, query)) {
                if (JSON.stringify(rates).indexOf(JSON.stringify(data)) === -1) {
                    rates.push(data);
                }
                return p.resolve();
            }
            return p.reject('Invalid data');

        }, (err) => {
            return p.reject(err.message);
        });
    }
    for (let d = moment(startDate); d < endDate; d.add(1, 'day')) {
        if (d.isoWeekday() !== 6 && d.isoWeekday() !== 7) {
            var p = q.defer();
            promises.push(p.promise);
            byDate(d.format('YYYY-MM-DD'), pQuery, query, p);
        }
    }

    q.all(promises).done(() => {
        var rate = new Rate();
        rate.bulkInsertFixer(rates, query, callback);
    }, err => {
        return callback(err);
    });

}