'use strict';
var http = require('http');
var url = require('url');
var _ = require('underscore');

function fixerQuery(path, method, query, callback) {
    var uri = url.parse(process.env.FIXER_API_URL);
    query = _.extend(query, { access_key: process.env.FIXER_API_KEY });
    var options = {
        hostname: uri.hostname,
        path: url.format({ pathname: uri.pathname + path, query: query }),
        headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
        },
        method: method
    };
    var req = http.request(options, function(res) {
        var response = '';

        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            response += chunk;
        });
        res.on('end', function() {
            try {
                var err = null;
                if (res.statusCode === 200) {
                    response = JSON.parse(response);
                } else {
                    err = true;
                }
                return callback(err, response);
            } catch (e) {
                return callback(true);
            }
        });
    });
    req.end();

    req.on('error', function(e) {
        return callback(new Error('Request ERROR'));
    });
}

function parseQuery(query) {
    return _.pick(query, 'base', 'symbols');
}

exports.symbols = (callback) => {
    fixerQuery('symbols', 'GET', {}, function(err, json) {
        if (err) {
            return callback(err);
        }
        return callback(false, json);
    });
};

exports.historical = (date, query = {}, callback) => {
    fixerQuery('historical/' + date, 'GET', parseQuery(query), function(err, json) {
        if (err) {
            return callback(err);
        }
        return callback(false, json);
    });
};

exports.latest = (query = {}, callback) => {
    fixerQuery('latest', 'GET', parseQuery(query), function(err, json) {
        if (err) {
            return callback(err);
        }
        return callback(false, json);
    });
};