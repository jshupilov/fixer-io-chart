'use strict';
var rates = require('./controllers/rates');
var symbols = require('./controllers/symbols');
var _ = require('underscore');

function requestFormatAndValidation(req, callback) {

    if (_.isObject(req)) {
        if (!_.isEmpty(req)) {
            var keys = ['query', 'params', 'headers', 'body'];
            var reqKeys = _.keys(req);
            for (let i in keys) {
                if (!_.contains(reqKeys, keys[i]) || !_.isObject(req[keys[i]])) {
                    return callback(true);
                }
            }
            return callback(null, {
                headers: req.headers,
                params: req.params,
                query: req.query,
                body: req.body
            });
        }
    }

    return callback(true);
}

function responseWrapper(err, data) {
    return {
        error: err,
        data: data
    }
}
module.exports = function(app) {

    // api ---------------------------------------------------------------------
    // get all rates
    app.get('/api/rates', function(req, res) {
        requestFormatAndValidation(req, (err, req) => {
            rates.getRates(req, (err, data) => {
                if (err) {
                    return res.json(responseWrapper(err));
                }
                return res.json(responseWrapper(null, data));
            });

        });
    });

    // get all symbols
    app.get('/api/symbols', function(req, res) {
        requestFormatAndValidation(req, (err, req) => {
            symbols.getSymbols(req, (err, data) => {
                if (err) {
                    return res.json(responseWrapper(err));
                }
                return res.json(responseWrapper(null, data));
            });

        });
    });

    // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendFile(__dirname + '/../public/index.html');
    });
}