'use strict';
var Symbols = require('../models/symbols');

exports.getSymbols = (req, callback) => {
    var symbols = new Symbols();
    symbols.find('all', callback);
};