'use strict';
var Rates = require('../models/rates');
var ratesFixer = require('../services/fixerApi');
var _ = require('underscore');
var q = require('q');
var moment = require('moment');
moment.locale('ee', {
    week: {
        dow: 1
    }
});

function validateQuery(query) {
    if (_.isObject(query)) {
        if (!_.isEmpty(query)) {
            var keys = ['start_date', 'end_date', 'base', 'symbols'];
            for (let i in keys) {
                if (!_.has(query, keys[i])) {
                    return false;
                }
            }
            const start_date = new Date(query.start_date);
            const end_date = new Date(query.end_date);
            if (_.isDate(start_date) && _.isDate(end_date)) {
                if (end_date > start_date) {
                    if (_.isString(query.base) && query.base.length === 3) {
                        if (_.isString(query.symbols) && query.symbols.length === 3) {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return false;
}

function checkIfDataExistsAdd(query, callback) {
    var rates = new Rates();
    rates.getRates(query, (err, data) => {
        if (err) {
            return callback(err);
        }
        if (data.length === 0) {
            ratesFixer.byPeriod(query, callback);
        } else {
            return callback(null, data);
        }
    });

}
exports.getRates = (req, callback) => {
    if (validateQuery(req.query)) {
        var query = req.query;
        var lastDay = moment(query.end_date, 'YYYY-MM-DD').add(1, 'week').startOf('week');
        var firstDay = moment(query.start_date, 'YYYY-MM-DD').startOf('week');
        var json = [];
        var promises = [];
        lastDay = lastDay > moment() ? moment() : lastDay;
        var doit = (query, p) => {
            checkIfDataExistsAdd(query, (err, data) => {
                if (err) {
                    return p.reject(err);
                }
                json = json.concat(data);
                return p.resolve();
            });
        };
        for (let d = firstDay; d < lastDay; d.add(1, 'week')) {
            var p = q.defer();
            var end = moment(d);
            promises.push(p.promise);
            query.start_date = d.format('YYYY-MM-DD');
            query.end_date = end.add(1, 'week').format('YYYY-MM-DD');
            doit(_.clone(query), p);
        }
        q.all(promises).done(() => callback(null, json), callback);
    } else {
        return callback('Query is invalid');
    }

};